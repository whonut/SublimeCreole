# SublimeCreole

A Sublime Text 3 syntax file for the [Creole](http://www.wikicreole.org/) wiki markup language.

Progress on implementation:

- [x] Headings (h1 through h6)
- [x] Bold
    - [x] In paragraphs
    - [x] In links
- [x] Italics
    - [x] In paragraphs
    - [x] In links
- [x] Links
    - [x] Raw
    - [x] Internal
    - [x] External
    - [x] Interwiki
- [x] Lists
    - [x] Ordered
    - [x] Unordered
- [x] Horizontal Rule
- [x] Images
- [x] Tables
- [x] Nowiki
- [x] Escape character
- [x] Placeholder